#!/bin/bash

wget https://raw.githubusercontent.com/GreatMedivack/files/master/list.out

grep Running list.out | cut -d ' ' -f1 > ${1:-defaultname}_$(date +%d_%m_%Y)_running.out
mkdir archives

if [ -f archives/${1:-defaultname}_$(date +%d_%m_%Y).tar ]
then
    echo "there is already an archive with this name"
else
    tar -czf ${1:-defaultname}_$(date +%d_%m_%Y).tar.gz ${1:-defaultname}_$(date +%d_%m_%Y)_running.out
    mv ${1:-defaultname}_$(date +%d_%m_%Y).tar.gz archives
fi

TAR=$(tar -xvzf archives/${1:-defaultname}_$(date +%d_%m_%Y).tar.gz -O)

if [ "$TAR" ]
then
    echo "the work was completed without errors"
else
    echo "the archive is damaged"
fi